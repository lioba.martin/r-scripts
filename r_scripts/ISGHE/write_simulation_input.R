# Info:
# File Purpose: Read ISGHE Data and Convert to Ldndc Formats
 Author <-  "Lioba Martin"
 Contact <-  "lioba.martin@kit.edu"
# Created: 21.03.2022
# Last Changed: 26.07.2022
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###
##  
# 
# Libraries                                                                                 ### 
library(tidyverse)
Sys.setlocale("LC_TIME","English")
###
#
# Info ##
#
# General Input Data                                                                       ####
#                                                                                           ###
# Location Data                                                                             ### 
id = 0
location_dat <- tribble(~"location"            ,~"dataset"    ,~"loc.code" ,~"ele",~"lat" ,~"lon"  ,~"tav",~"tamp",
#                        "Maricopa, AZ, USA", "Matre 2018" , "US_maricopa",361   ,33.069 ,-114.53,
                        "Ciudad Obregon, Mexico"  , "Matre 2017" , "MXOB" ,38    ,27.397 ,-109.924,25.9  ,6.5,
                        "Tlatizapan, Mexico","Matre 2017", "MXTL" ,940   ,18.69  ,-99.126 ,26.3  ,3.1, 
                        "Wad Medani, Sudan", "Matre 2017" , "SUWM" ,411   ,14.404 ,33.49   ,29.3  ,4.5,
                        "Dharwar, India"   , "Matre 2017" , "INDH" ,638   ,15.485 ,74.977  ,26.1  ,3.3,
                        "Dinajpur, Bangladesh","Matre 2017", "BDDI" ,29    ,25.65  ,88.68   ,25.6  ,5.6,
                        "Aswan, Egypt"     , "Matre 2017" , "EGAS" ,200   ,24.1   ,32.9    ,27.1  ,8.7) %>% 
  mutate(prefix = paste0("ISGHE_",loc.code))

#                                                                                           ###


#                                                                                           ###
#Information Input                                                                          ###   
input <- "C:/Users/martin-l/Documents/LandscapeDNDC/input/AgMIP_Datasets/ISGHE/"
setwd(input)
#
# Where to store the simulation Input
simulation.folder <- "C:/Users/martin-l/Documents/LandscapeDNDC/projects/"
simulation.in <- paste0(simulation.folder,"arable/ISGHE/")
for(i in location_dat$prefix)  if(!dir.exists(paste0(simulation.in,i))) dir.create(paste0(simulation.in,i))
# 
#                                                                                           ###
## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ##



### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###
# General Information 
setwd(input)
file.siteinfo <- "1-Soil and crop mangement/ISGHE_AgMIP_treatment_key.txt"


# Management codes
siteinfo <- read.delim(file.siteinfo) %>% tibble %>% janitor::clean_names() %>% janitor::remove_empty(which = "cols")

siteinfo <- right_join(location_dat,siteinfo) %>% 
  select(location,loc.code,prefix,x_treatment,year,cultivar) %>% 
  rename(trt = x_treatment) %>%
  mutate(spec.name = str_replace(cultivar," ","_"))


### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###


### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###
##                                                                                          ###
# Soil Data                                                                                ####
##                                                                                          ###
### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###

# Use the Maricopa Soil File

setwd(simulation.folder)
for(prefix in location_dat$prefix){
  if(!file.exists(paste0(simulation.folder,"arable/ISGHE/US_maricopa_site.xml"))){
    file.copy(paste0(simulation.folder,"arable/US_maricopa/US_maricopa_site.xml"),paste0(simulation.folder,"arable/ISGHE/US_maricopa_site.xml"))
  }
}


### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###
##                                                                                          ###
# Climate Data                                                                             ####
##                                                                                          ###
### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###

# load function to convert a climate table into ldndc input
load("~/Documents/R_scripts/functions/write_climate_txt.Rdata")

# Should the simulation input be stored in a subfolder? 
sub = NA

# Input Climate Data    ### ### ### ###

# create loops for the different climate files in the Input Data
folder.weather <- "3-Weather data/"

### ### ### ### ### ### ### ### ### ###


## ## ## ## ##

# General Site Climate 
precip.ann = 164 # Annual Precipitation ## Source: Martre et al. 2017		
temp.ann = 21.0 # Mean annual Temperature
temp.var.ann = 11.1 # Mean annual variation of Temperature




## Read Climate Files and save to list
weather.id <- list.files(path = folder.weather,pattern = ".WTH") %>% gsub(".WTH",.,replacement = "")


for(i in weather.id){
  info <- location_dat %>% filter(paste0(loc.code,"0001") == i)
  

  file.clim.dat <- paste(folder.weather,i,".WTH",sep = "")
  clim.dat <- read.table(file.clim.dat,
                         comment.char = "!",skip = 4,header = T) %>% tibble 
  
  # Separate YYDOY into two columns with Year and DOY (problem is that a separate only works with positions for numerics, and if the first digit is 0 it is lost, so 03001 -> 3001, thats why distinction by number of characters is necessary)
  if (clim.dat$X.DATE %>% tail(1) %>% nchar == 4) {
    clim.dat <- clim.dat %>% separate(X.DATE,into = c("Year","DOY"),sep = 1,convert = T)
  } else if (clim.dat$X.DATE %>% tail(1) %>% nchar == 5){
    clim.dat <-  clim.dat %>% separate(X.DATE,into = c("Year","DOY"),sep = 2,convert = T)
  }
  
  #Convert one or two digit year into YYYY
  clim.dat$Year <- ifelse(clim.dat$Year < 30,yes = clim.dat$Year <- clim.dat$Year + 2000,clim.dat$Year <- clim.dat$Year + 1900)
  
  #Convert DOY and Year to Date (note that R uses a 0 based index for dates, hence DOY-1)
  clim.dat$Date <- as.Date(clim.dat$DOY-1,origin = paste(clim.dat$Year,"-01-01",sep = ""))
  # Convert the Radiation from MJ/m-2/a in W/m-2/s
  clim.dat <- clim.dat %>% mutate(SRAD = SRAD*11.57)
  # Adjust colum names and drop the ones not used in LDNDC
  clim.dat <- clim.dat %>% rename_with(str_to_lower) %>% 
    rename(grad = srad,prec = rain,'*wind' = wind) %>% select(date,everything()) %>% select(-doy,-year)
  

  # convert into LDNDC compatible file 
  clim.out <- write.climate(clim.dat,id = 0,ele = info$ele,lon = info$lon, lat = info$lat,timestep = 1,temp.ann = info$tav,temp.var.ann = info$tamp)
  
  # Where to save to 

  folder.clim.sim <- ifelse(is.character(sub),
                            paste(simulation.in,info$prefix,sub,"/",sep = ""),
                            paste(simulation.in,info$prefix,sep = ""))
  if(!dir.exists(folder.clim.sim)) dir.create(folder.clim.sim)
  
  
  file.clim.sim <- paste(folder.clim.sim,"/",info$prefix,"_climate",".txt",sep = "")
  write.table(clim.out,file.clim.sim,sep = "\t",quote = F,row.names = F,col.names = F,na = "")
}


### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###
##                                                                                          ###
# Events                                                                                   ####
##                                                                                          ###
### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###

# Input Event Data ### 
setwd(input)

# Planting
file.plant <- "1-Soil and crop mangement/ISGHE_AgMIP_crop_management_events.txt"

# Should the event(s) be stored in a subfolder? If so, give the suffix the subfolder should have (syntax = prefix+suffix)
sub <- NA


### ### ### ### ### ### ### ### ### 

## functions ###


source("C:/Users/martin-l/Documents/R_scripts/events_to_html.R")

# Read Planting Data ## ## ##
plant.dat <- read.delim(file.plant,header = T, sep = "\t",check.names = F) %>% janitor::clean_names() %>% tibble %>%
  mutate(code = "WIWH",
         #seednum = plant_population_number_m2*10000,
         biomass = 150,
         date = as.Date(sowing_date_estimated_from_emergence_date_7_days_before_emergence_yyyy_mm_dd),
         trt = treatment) %>% 
  select(biomass,code,trt,date)


# Join with location and heating code data
plant.dat <- left_join(siteinfo,plant.dat,by = c("trt")) 

## Add the Spinup Data to the Planting Data
#plant.dat <- bind_rows(spinup.planting,plant.dat)

# Convert to HTML
plant.out <- planting(date = plant.dat$date,name = plant.dat$spec.name,type = plant.dat$code,initbiom = 150)
plant.dat$html <- plant.out

## ## ## ## ## ## ##

# Harvest Data ## 

## ## ## ## ## ## ##

## Fertilization Data ###


## ## ## ## ## ## ##

# Read Irrigation Data ## ## ##

## ## ## ## ## ## ##


# Event Simulation Input ###



# Sorted by Location and written into separate files for management 
event.treat <- bind_rows(planting = plant.dat,.id = "mangament") %>% select(date,code,cultivar,location,html)


location <- siteinfo$loc.code %>% unique
cultivar <- siteinfo$spec.name %>% unique

for (l in location){
  for(c in cultivar){
  
    info <- siteinfo %>% filter(loc.code == l,spec.name == c) %>% select(location, loc.code, prefix,spec.name) %>% unique()
    event.out.treat <- c("<?xml version = \"1.0\" encoding = \"UTF-8\" ?> 
                  <ldndcevent> \n",
                       paste("<event id = \"",id,"\" > \n",sep = ""),
                       event.treat %>% filter(location == l,cultivar == c) %>% pull(html),
                       "</event> 
                 </ldndcevent>")
    
    
  # Where to save to 
    folder.event.sim <- ifelse(is.character(sub),
                               paste(simulation.in,info$prefix,sub,"/",sep = ""),
                               paste(simulation.in,info$prefix,"/",sep = ""))
    
    setwd(simulation.folder)
    if(!dir.exists(folder.event.sim)) dir.create(folder.event.sim,recursive = T)
    
    
  write.table(event.out.treat,
              paste(folder.event.sim,info$prefix,"_",c,"_mana.xml",sep = "")
              ,quote = F, row.names = F,col.names = F)
  
  message("Wrote event file for ",info$location,"; Cultivar: ",info$spec.name)
  }
}


### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###
##                                                                                          ###
# ldndc files                                                                              ####
##                                                                                          ###
### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###

# Do the LDNDC Files for each Country and each Cultivar -> 8 Cominations

setwd(simulation.folder)

location <- siteinfo$loc.code %>% unique
cultivar <- siteinfo$spec.name %>% unique

for(l in location){
  for(c in cultivar){
  
    
  name <- location_dat %>% filter(loc.code == l) %>% pull(location)
  dataset <- location_dat %>% filter(loc.code == l) %>% pull(dataset)
  info <-  siteinfo %>% filter(loc.code == l & spec.name == c) %>% select(-year,-trt) %>% unique
  prefix <- info$prefix 
  year.range <- siteinfo %>% filter(loc.code == l & spec.name == c) %>% select(year) %>% range
  
  simulation.out <- paste0(prefix,"_",info$spec.name,"_output/")
  sinks <- paste(simulation.in,prefix,"/",prefix,"_",info$spec.name,"/",simulation.out,"_",prefix,"_",info$spec.name,"_",sep = "")

  content.ldndc <- paste('<?xml version="1.0" encoding = "UTF-8"?>
    <ldndcproject PackageMinimumVersionRequired="1.0" >
    
    \t <description>
    \t \t <name>',name,' </name>
    \t \t <author>',Author,'</author>
    \t \t <date> ',as.character(Sys.Date()),'</date>
    \t \t <email> ',Contact,'</email>
    \t \t <dataset> ',dataset,' </dataset>
    \t </description>
    
    \t <schedule  time="',year.range[1],'-01-01/24',' -> ',year.range[2],'-12-31" />
  
    \t <input>
    \t \t <sources  sourceprefix=',paste('"',simulation.in,prefix,"/",prefix,'"',sep = ""),' >
    \t \t \t <setup   source="_setup.xml" />
    \t \t \t <climate source="_climate.txt" />
    \t \t \t <site              sourceprefix="',paste0(simulation.in,"US_maricopa"),'" source="_site.xml" />
    \t \t \t <siteparameters    sourceprefix="',paste0(simulation.in,"ISGHE"),'" source="_siteparameters.xml" />
    \t \t \t <speciesparameters sourceprefix="',paste0(simulation.in,"ISGHE"),'" source="',paste0("_speciesparameters_",c,".xml"),'" />
    \t \t \t <airchemistry      sourceprefix="',paste0(simulation.in,"ISGHE"),'" source = "_airchem.txt" />
             <event  source=',paste0('"_',c,'_mana.xml"'),'/>
    \t \t </sources>
    \t \t <attributes  use="0" endless="0" >
	            <airchemistry  endless="yes" />
	            <climate  endless="no" />
	        </attributes>
    \t </input>
    
    \t <output>
    \t \t <sinks sinkprefix= ',paste('"',sinks,'"',sep = ""),' />
    \t </output>
    
    </ldndcproject>',sep = "")
  
  
  folder.run <- paste(simulation.in,prefix,"/",sep = "")
  if(!dir.exists(folder.run)) dir.create(folder.run)
  
  file.ldndc <- paste(folder.run,prefix,"_",info$spec.name,".ldndc",sep = "")
  
  write.table(content.ldndc,file.ldndc,quote = F,col.names = F, row.names = F)
  message("wrote ",file.ldndc)
  }
}


### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###
##                                                                                          ###
# setup file                                                                               ####
##                                                                                          ###
### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###

for(l in location){
  
  info <- location_dat %>% filter(loc.code == l)
  content.setup <- paste('<?xml version="1.0" encoding = "UTF-8"?>
    <ldndcsetup>
    <setup id="0" name="',name,'" comment= "no comment " >
        <location elevation="',info$ele,'" longitude="',info$lon,'" latitude="',info$lat,'" slope="',0,'" />
        <models>
            <model id="FarmSystem" />
            <model id="_MoBiLE" /> 
        </models>
        <FarmSystem file="',paste0("%I/arable/ISGHE/ISGHE_MXOB/ISGHE_MXOB_Farmsystem",".json"),'" id="0"/> 
        <mobile>
            <modulelist>
                <module id="microclimate:canopyecm"         timemode="subdaily"/>
                <module id="watercycle:watercycledndc"      timemode="subdaily"/>
                <module id="physiology:plamox"              timemode="subdaily"/>
                <module id="soilchemistry:metrx"            timemode="subdaily" />
                
				        <module id="output:microclimate:daily" />
                <module id="output:physiology:daily" />
                <module id="output:watercycle:daily" />
                <module id="output:soilchemistry:daily" />
				        <module id="output:report:arable:harvest" timemode="subdaily" />
            </modulelist>
        </mobile>
        <use>
            <site  id="',id,'" />
            <climate  id="',id,'" />
            <event  id="',id,'" />
            <siteparameters  id="',id,'" />
            <speciesparameters id="',id,'" />
        </use>
    </setup>
  </ldndcsetup>',sep = "")


file.setup <- paste(simulation.in,info$prefix,"/",info$prefix,"_setup.xml",sep = "")
write.table(content.setup,file.setup,quote = F,col.names = F, row.names = F)

message("wrote Setup File for ",info$location)
}

### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###
##                                                                                          ###
# Airchemistry                                                                             ####
##                                                                                          ###
### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###

# Use the Maricopa Soil File

setwd(simulation.folder)
for(prefix in location_dat$prefix){
  if(!file.exists(paste0(simulation.folder,"arable/ISGHE/ISGHE_arichem.txt"))){
    file.copy(paste0(simulation.folder,"arable/ISGHE/US_maricopa_airchem.txt"),paste0(simulation.folder,"arable/ISGHE/ISGHE_arichem.txt"))
  }
}



### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###
##                                                                                          ###
# batch  file                                                                              ####
##                                                                                          ###
### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###


siteinfo <- read.delim("C:/Users/martin-l/Documents/LandscapeDNDC/InputData/AgMIP_Datasets/HSC-AgMIP_data/HSC_AgMIP_treatments_locationwise.txt")

# Filter the treatments for those where a heated and unheated were done on the same date
heated <- siteinfo %>% filter(heating == "H") %>% pull(plant.date)

siteinfo <- siteinfo %>% 
  filter(plant.date %in% heated) %>% select(treatments,contains("_id")) %>%
  distinct()


pre <- paste("cd C:/Users/martin-l/Documents/ldndc_work/projects" ,sep = "")
batch <- ""
for(t in 1:nrow(siteinfo)){
  batch <- paste(batch,
  ".\\ldndc.exe -c ldndc.conf arable\\US_maricopa\\US_maricopa_",
  siteinfo$treatments[t],"\\",prefix,"_",siteinfo$treatments[t],".ldndc \n",sep = "")
}

write.table(c(pre,batch,"\n PAUSE"),file = "~/Documents/ldndc_work/projects/arable/US_maricopa/US_maricopa.bat"
            ,quote = F, col.names = F, row.names = F)

##  Remove Stuff ##

for(l in location){
  for(c in cultivar){
    info <- siteinfo %>% filter(loc.code == l & spec.name == c) %>% select(-year,-trt) %>% unique
    prefix <- info$prefix
    unlink(paste0(simulation.folder,"arable/ISGHE/",prefix,"/US_maricopa_site.xml"))
  }
}
