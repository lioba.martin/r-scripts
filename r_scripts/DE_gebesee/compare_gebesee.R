## Info:
# File Purpose: Compare the LDNDC Simulations with the Measured Data from the DE_Gebesee Site
# Author: Lioba Martin
# Contact: lioba.martin@kit.edu
# Created: 26.07.2022
# Last Changed: 29.09.2022
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# Things that need to be adapted:

source("~/Documents/R_scripts/DE_gebesee/Gebesee_general.R")

multiple_runs <- "metrx"

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

## Libraries

library(ggplot2)
library(tidyverse)
library(lubridate)
library(janitor)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


## Read in standard-format measurements ## ## ## ## ##

measurement.files <- paste0(prefix,"_",c("NEE","Reco","ET","SWC","TS","agb")
                            ,".txt")

source("~/Documents/R_scripts/measurements_to_R.R")


## read in  LandscapeDNDC Model Output ## ## ## ## ##

simulation.files <- c(paste0(prefix,"_",multiple_runs,"_",c("ecosystem","microclimate","physiology","soilchemistry","watercycle"),"-daily.txt")
                      #,paste0(prefix,"_",multiple_runs,"_",c("physiology","soilchemistry","watercycle"),"-subdaily.txt")
                      )


source(paste0(Rscripts,"simulation_output_to_R.R"))


### calculate variables not automatically created by LDNDC ####

# 1. Calculate and introduce as new column in a data frame
watercycle_daily <- watercycle_daily %>% mutate(eva = evacep + evasoil + evasurfacewater + transp)
# 2. Pass the necessary information for plotting into the list.vars data frame. File destination does not have to be specified, and can be left blank.
list.vars <- bind_rows(list.vars,
                       tibble(variable = "eva",unit = "mm", table = "watercycle-daily",run = multiple_runs))%>% filter(table != "report-harvest")



## read in Meteo Data ## ## ## ## ## ## ## ## ## ## ##

climate <- read.climate(paste0(simulation.in,"DE_gebesee_climate.txt"))
# daily climate
climate_d <-
  climate %>%
  group_by(date = as.Date(date)) %>%
  summarize(tavg = mean(tavg),prec = sum(prec),tmin = min(tmin),tmax = max(tmax),grad = mean(grad),rh = mean(rh))





### combine LDNDC output tables ####

LDNDC <- full_join(physiology_daily,watercycle_daily,by = c("datetime","source","id","run")) %>%
  full_join(.,soilchemistry_daily,by = c("datetime","source","id","run")) %>%
  mutate(dC_co2_emis_auto = dC_co2_emis_auto/10000,dC_co2_emis_hetero = dC_co2_emis_hetero/10000, # Adjusting the Units -> kgC/ha -> kgC/m2
         NEE_calc =  dC_co2_emis_hetero + (dC_growth_resp + dC_maintenance_resp + dC_transport_resp) -  dC_co2_upt) %>%
  full_join(.,climate_d,by = c("datetime" = "date")) %>%
  right_join(.,ecosystem_daily,by = c("datetime","source","id","run"))

#x <- LDNDC %>% filter(year(datetime) %in% c(2019:2021)) %>% select(datetime,dC_NEE)

#y <- ecosystem_daily %>% filter(year(datetime) %in% c(2019:2021)) %>% select(datetime,dC_NEE)
## Combine Subdaily Data for NEE
# Data needs some thinning, because due to subdaily is quite large, so only combine "necessary" data
LDNDC_NEE <-
physiology_subdaily %>%
  select(datetime,species,contains("resp"),dC_co2_upt) %>% #units all kgC m-2 ts-1 (ts = timestep, also h)
full_join(.,  soilchemistry_subdaily %>%
            select(datetime,contains("co2_")) #already in kgC:m-2, no need for conversion
          ,by = "datetime") %>%
  mutate(NEE =  sC_co2_hetero + (dC_growth_resp + dC_maintenance_resp + dC_transport_resp) -  dC_co2_upt) %>% # should be in kgC:m-2:h
  select(datetime,species,NEE) %>%
  right_join(climate,by = c("datetime" = "date"))


## Extract Species on field via the AGB measurements

species.clean <-  agb %>%
  filter(info != "Winter wheat pre-harvest AGB sampling") %>%
  mutate(datetime = as.Date(datetime-1)) %>%
  left_join(.,LDNDC,by = "datetime") %>% pull(species_long) %>% str_replace("_"," ") %>% unique

species.ldndc <-  agb %>%
  filter(info != "Winter wheat pre-harvest AGB sampling") %>%
  mutate(datetime = as.Date(datetime-1)) %>%
  full_join(.,LDNDC %>% select(datetime,species),by = "datetime") %>%  drop_na(species) %>%
  pull(species) %>% unique

## Convert speies columns to these names
LDNDC <-
LDNDC %>%
  mutate(species = factor(species,levels = species.ldndc, labels = species.clean))


LDNDC_NEE <-
  LDNDC_NEE %>%
  mutate(species = factor(species,levels = species.ldndc, labels = species.clean))
# make_clean_names is computationally expensive, takes to long for subdaily

## ### ### ### ### ### ### ### ###
## Net Ecosystem Exchange NEE ####

#daily
compare_NEE <-
 right_join(NEE,LDNDC,by = c("datetime")) %>%
  mutate(NEE_EC = NEE *(60*30*24) * 12 /1000000 ,  #µmolCo2:m-2:s-1 -> gC:m-2:d-1 (60seconds*1/2 hour*24hours) * M(co2 = 12g/mol) * 10^-6 (mikro -> gramm)
         NEE_ldndc = -dC_NEE/10, # kgC:ha -> gC:m-2 [*10^-4 (ha -> m2) *1000 (kg -> g)]
         NEE_ldndc2 = NEE_calc*1000) %>% # kgC:m-2:d-1 -> gC:m-2d-1
  select(datetime,contains("NEE"),species,-NEE,-NEE_std,-dC_NEE,-NEE_calc,run)


# ## Accumulate NEE
# NEE_cum <-
# compare_NEE %>% group_by(species) %>%
#   mutate(days = 1) %>%
#   summarise("NEE_ldndc_cum" = cumsum(NEE_ldndc),
#             "NEE_EC_cum" = cumsum(NEE_EC)) %>%
#   ungroup() %>% select(-species) %>%
#   bind_cols(compare_NEE,.)
# 

# subdaily
compare_sNEE <-
right_join(NEE,LDNDC_NEE,by = "datetime",suffix = c("_ec","_ldndc")) %>%
  rename(NEE_ec_sd = NEE_std) %>%
  mutate(NEE_ec = NEE_ec * 12 /1000000 * 60*60, #µmolCo2:m-2:s-1 -> gC:m-2:h-1
         NEE_ec_sd = NEE_ec_sd  * 12 /1000000 * 60*60,
         NEE_ldndc = NEE_ldndc * 1000) # kgC:m-2 -> kgC:m-2:ha

### ### ### ### ### ### ### ### ###
### Ecosystem Respiration Reco ####


compare_Reco <-
  right_join(Reco,LDNDC,by = c("datetime")) %>%
  mutate(Reco_EC = Reco/1000, Reco_ldndc = dC_growth_resp) %>%
  select(datetime,contains("Reco"),species,-Reco,-Reco_std,run)


### ### ### ### ### ### ### ### ###
### Evapotranspiration         ####


compare_ET <-
  right_join(ET,LDNDC,by = "datetime") %>%
  mutate(ET_EC = ET/1000, ET_ldndc = eva) %>%
  select(datetime,contains( "ET_"),species,-ET,-ET_std,run)

### ### ### ### ### ### ### ### ###
### Soil Water Content         ####

compare_swc <-
right_join(SWC,LDNDC,by = "datetime") %>%
  select(datetime,species,contains("SWC"),contains("mean"),-contains("std"),
         soilwater_5cm,soilwater_10cm,soilwater_20cm,run) %>%
  rename_at(vars(contains("soilwater")),function(x) paste0(x,"_ldndc")) %>%
  rename_at(vars(contains("_mean")),function(x) paste0(x,"_EC"))


## ### ### ### ### ### ### ### ###
## Above Ground Biomass       ####



compare_agb <-
  agb %>%
  filter(info != "Winter wheat pre-harvest AGB sampling") %>%
  mutate(datetime = as_date(datetime)-1) %>%
  left_join(.,LDNDC,by = "datetime") %>%
  mutate(AGB_EC = agb, AGB_ldndc = DW_above*10, #kg/m² -> T/ha (*10)
         yield_EC = yield, yield_ldndc = DW_fru*10) %>% #kg/m² -> T/ha (*10)
  select(datetime,species,contains("AGB"),contains("yield"),-agb,-yield,run)



# ## Combine Data ###

# Daily Timestep

compare <-
  climate_d %>% rename(datetime = date) %>%
  full_join(.,compare_ET) %>% 
  full_join(.,compare_Reco) %>%
  full_join(.,compare_NEE) %>%
  #full_join(.,NEE_cum) %>%
  full_join(.,compare_swc) %>%
  full_join(.,compare_agb) %>%
  mutate(year = year(datetime)) %>%
  relocate(datetime,species) %>%
  mutate(run = ifelse(run %>% nchar %>% sum(na.rm = T) == 0, "ldndc", run))

# Subdaily Timestep 

s_compare <- 
  climate %>% rename(datetime = date) %>%
  full_join(compare_sNEE) %>%
  relocate(datetime,species)


