---
title: "Klingenberg Yields"
output: html_notebook 
---

```{r Libraries, include = FALSE}
## Libraries
library(tidyverse)
library(sp)
#library(maptools)
library(knitr)
library(plotly)


knitr::opts_chunk$set(dev = "png",
                      echo = T)
# Show point shapes in R:
#ggpubr::show_point_shapes()
#ggpubr::show_line_types()

source("~/Documents/R_scripts/DE_klingenberg/compare_klingenberg.R",echo = T)

theme_kkplot <- function(){theme_minimal() %+replace%
    theme(legend.position = "top",#c(0.8,0.9),
          legend.background = element_blank(),
          legend.direction = "horizontal",
          panel.grid.minor = element_blank())
}
```

#### Biomass and Yield


```{r Biomass, warning = F}
crops %>% select(crop,Sowing_date,harvest_date) %>% 
  filter(crop %in% c("maize", "winter wheat")) %>% 
  group_by(crop) %>% kable



compare_biomass %>% select(-crop,-contains("yield")) %>%
  right_join(.,croptime,by = c("datetime" = "date")) %>%
  pivot_longer(cols = contains("AGB"),names_to = "method",values_to = "AGB") %>%
  mutate(method = case_when(str_detect(method,"obs")  ~ "obs",
                            str_detect(method,"ldndc") ~ "ldndc")) %>%
  filter(crop == "winter wheat") %>%
  ggplot(aes(dof,AGB,color = method,shape = method)) +
  geom_point() +
  labs(y = "AGB (kg/m-2)", x = "",title = "Wheat AGB") +
  #scale_x_datetime(breaks = "1 year",date_labels = "%Y") +
  theme_kkplot() +
  scale_color_manual(values = c("red","black"),
                     labels = c("obs","ldndc"),
                     breaks = c("obs","ldndc"),
                     name = "") +
  scale_shape_manual(values = c(4,20),
                     labels = c("obs","ldndc"),
                     breaks = c("obs","ldndc"),
                     name = "") +
  facet_grid(.~rep)


compare_biomass %>% select(-crop,-contains("yield")) %>%
  right_join(.,croptime,by = c("datetime" = "date")) %>%
  pivot_longer(cols = contains("AGB"),names_to = "method",values_to = "AGB") %>%
  mutate(method = case_when(str_detect(method,"obs")  ~ "obs",
                            str_detect(method,"ldndc") ~ "ldndc")) %>%
  filter(crop == "maize") %>%
  ggplot(aes(dof,AGB,color = method,shape = method)) +
  geom_point() +
  labs(y = "AGB (kg/m-2)", x = "",title = "Maize AGB") +
  #scale_x_datetime(breaks = "1 year",date_labels = "%Y") +
  theme_kkplot() +
  scale_color_manual(values = c("red","black"),
                     labels = c("obs","ldndc"),
                     breaks = c("obs","ldndc"),
                     name = "") +
  scale_shape_manual(values = c(4,20),
                     labels = c("obs","ldndc"),
                     breaks = c("obs","ldndc"),
                     name = "") +
  facet_grid(.~rep)


compare_biomass %>% select(-crop,-contains("AGB")) %>%
  right_join(.,croptime,by = c("datetime" = "date")) %>%
  pivot_longer(cols = contains("yield"),names_to = "method",values_to = "yield") %>%
  mutate(method = case_when(str_detect(method,"obs")  ~ "obs",
                            str_detect(method,"ldndc") ~ "ldndc")) %>%
  filter(crop == "winter wheat") %>%
  ggplot(aes(dof,yield,color = method,shape = method)) +
  geom_point() +
  labs(y = "yield (kg/m-2)", x = "",title = "Winter wheat Yield") +
  theme_kkplot() +
  scale_color_manual(values = c("red","black"),
                     labels = c("obs","ldndc"),
                     breaks = c("obs","ldndc"),
                     name = "") +
  scale_shape_manual(values = c(4,20),
                     labels = c("obs","ldndc"),
                     breaks = c("obs","ldndc"),
                     name = "") +
  facet_grid(.~rep)
```

## NEE Development

```{r}

compare_NEEd %>%
  right_join(.,physiology_daily %>% select(datetime,gdd)) %>%
  filter(crop == "maize") %>%
  ggplot(aes(x = gdd)) +
  geom_point(aes(y = NEE_EC_cum,color = "EC")) +
  geom_point(aes(y = NEE_ldndc_cum,color = "ldndc")) +
  facet_grid(.~rep) +
  theme_kkplot()


```
DVS Flush
```{r}
physiology_daily %>%
  right_join(.,croptime,by = c("datetime" = "date")) %>%
  filter(crop == "maize",dof >= 155) %>% select(rep,datetime,dof,gdd)
ggplot() +
  geom_point(aes(dof,dvs_flush,color = gdd)) +
  facet_grid(rep~crop) +
  guides(color = F)
```

