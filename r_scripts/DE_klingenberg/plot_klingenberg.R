
source("~/Documents/R_scripts/DE_klingenberg/compare_klingenberg.R")


folder.plots <- paste0("C:/Users/martin-l/Documents/LandscapeDNDC/Comparson_plots/",prefix,"/")

setwd(folder.plots)

library(patchwork) # Arrange plots: patchwork
library(ggnewscale) # Two different scales
library(plotly)
library(webshot)

# Show point shapes in R:
#ggpubr::show_point_shapes()

### themes & Scales ####

scale_color_kkplot <- function() scale_color_manual(values = c("red","black","blue"),
                                                    labels = c("obs",if_else(multiple_runs != "",as.character(multiple_runs),"ldndc")),
                                                    breaks = c("obs",if_else(multiple_runs != "",as.character(multiple_runs),"ldndc")),
                                                    name = "")
theme_kkplot <- function(){theme_minimal() %+replace%
    theme(legend.position = "top",#c(0.8,0.9),
          legend.background = element_blank(),
          legend.direction = "horizontal",
          panel.grid.minor = element_blank())
}

### ### ### ### ### ### ### ### ### ###
## Plot Selected output Variables  ####

# Select key variables to plot and select the style (p = point, l = line, h = columns)
key.vars <- tribble(~variable,~style,
                    "dC_co2_upt","l",
                    "dC_growth_resp","l",
                    "eva","l",
                    "DW_above","p",
                    "dvs_flush","p")

#create a table with the units of the key variables from "list.vars"
table <- left_join(key.vars,list.vars,by = "variable") %>% filter(table != "report-harvest")

#daterange <- date_range(spinup_years = 0,date_vec = physiology_daily$date %>% as.Date)

table <- table %>% filter(!str_detect(table,"subdaily"))

# Plot the table of variables
for (t in table$variable){
  info <- table %>% select(variable,style,unit,table) %>% unique %>% filter(variable == t)
  p <- plot.var(var = t,info = info)
  assign(t,p)
}


(dC_co2_upt/DW_above/eva) | (dC_growth_resp / dvs_flush)


### ### ### ### ### ### ### ### ###
### Net Ecosystem Exchange NEE ####

plot_NEE <-
compare_NEEd %>%
  select(datetime,crop,contains("NEE"),-contains("cum")) %>%
  pivot_longer(cols = contains("NEE")) %>%
  filter(species == "WIWH") %>%
  ggplot(aes(datetime,value,color = name)) +
  geom_point() +
  labs(y = "NEE (kgC m-2 d-1)") +
  theme_kkplot()+
  labs(color = "")
plot_NEE

#ggsave(paste0(folder.plots,"NEE.png"),plot_NEE,width = 20, height = 5,bg = "white")

### Like KKplot
ggplotly(
compare %>%
  select(date,species,contains("NEE"),-contains("cum"),run,cycl) %>%
  #filter(cycl %>% str_detect("wiwh")) %>%
  ggplot(aes(x = date)) +
  geom_point(aes(y = NEE_EC, color = "obs"),size = 0.5) +
  geom_line(aes(y = NEE_ldndc, color = run)) +
  labs(x = "",
       y = "NEE (kgC m-2 d-1)") +
  scale_color_kkplot() +
  theme_kkplot())


ggsave(paste0(folder.plots,"NEE_kkplot_Poster.png"),width = 8, height = 2,bg = "white")


compare_NEEd %>%
  right_join(.,physiology_daily %>% select(datetime,gdd)) %>%
  filter(crop == "maize") %>%
  ggplot(aes(x = gdd)) +
  geom_point(aes(y = NEE_EC_cum,color = "obs")) +
  geom_point(aes(y = NEE_ldndc_cum,color = "ldndc")) +
  facet_grid(.~rep) +
  theme_kkplot() +
  scale_color_kkplot()
ggsave(paste0(folder.plots,"NEE_compare_6.png"),width = 8, height = 2,bg = "white")

## Cumulative NEE with yield

compare %>%
  mutate(doy = yday(date),AGB_EC = AGB_EC/10*0.45,yield_EC = yield_EC/10*0.45,NEE_ldndc_cum = -NEE_ldndc_cum, NEE_EC_cum = -NEE_EC_cum) %>%
  filter(cycl %>% str_detect("wiwh")) %>%
  pivot_longer(cols = c("NEE_ldndc_cum","NEE_EC_cum","AGB_EC","yield_EC")) %>%
  ggplot(aes(date,value,color = name)) + geom_point() +
  scale_x_date(date_breaks = "1 year",date_labels = "%Y",minor_breaks = NULL) +
  geom_point(data= report_harvest %>% mutate(dC_tot = (dC_fru/10000 + dC_straw/10000 + dC_frt/10000)),aes(date,dC_tot,color = "yield_LDNDC")) +
  theme_minimal() #+
  scale_x_date(limits = c(as.Date("2000-10-01"),as.Date("2001-08-30")))

  #facet_grid(cycl~.)



### ### ### ### ### ### ### ### ###
### Ecosystem Respiration Reco ####


# What is actually dC_TER in ldndc? 

right_join(Reco,physiology_daily) %>%
  full_join(.,ecosystem_daily) %>%
  subset(year(datetime) == "2020") %>%
  #pivot_longer(cols = c("dC_fol_resp","dC_fru_resp","dC_frt_resp","dC_lst_resp"),names_to = "div",values_to = "resp") %>%
  pivot_longer(cols = c("dC_transport_resp","dC_maintenance_resp","dC_growth_resp"),names_to = "div",values_to = "resp") %>%
  ggplot(aes(datetime)) +
  geom_area(aes(y = resp,fill = div,col = div)) +
  geom_point(aes(y = dC_TER/10000,col = "dC_TER",fill = "dC_TER"),size = 2) 


### ### ### ### ### ### ### ### ###
### Gross Primary Productivity ####

# hourly, only EC
plot_GPP <-
  GPP %>%
  ggplot(aes(datetime,GPP *(60*60)* 12 /1000000)) +
  #facet_grid(name~.) +
  geom_point() +
  labs(y = "GPP (gC m-2 h-1)") +
  scale_x_datetime(breaks = "1 year",date_labels = "%Y") +
  labs(color = "") +
  theme(legend.position = c(.9,.9)) +
  theme_minimal()
plot_GPP


# daily, EC vs. LDNDC
compare_GPPd %>%
  ggplot(aes(datetime)) + 
  geom_point(aes(y = GPP_EC,col = "EC")) +
  geom_point(aes(y = GPP_ldndc,col = "ldndc")) +
  labs(y = "GPP (gC m-2 d-1)",x = "",col = "") +
  theme_kkplot()

### ### ### ### ### ### ### ### ###
### Evapotranspiration         ####

# hourly, only EC
plot_ET <-
ET %>% #filter(year(datetime) == 2004) %>%
  ggplot(aes(datetime,ET)) +
  geom_point() +
  labs(y = "ET (mm)") +
  #facet_grid(name~.) +
  scale_x_datetime(breaks = "1 year",date_labels = "%Y") +
  labs(color = "") +
  theme(legend.position = c(.9,.9)) +
  theme_minimal()
plot_ET 


## hourly, ldndc vs. EC##
compare_ET  %>% #filter(datetime >= as.Date("2021-01-08")) %>% 
  ggplot(aes(x = datetime)) +
  geom_point(aes(y = ET_EC, color = "obs")) +
  geom_point(aes(y = ET_ldndc, color = "ldndc")) +
  labs(x = "",
       y = "ET mm") +
  scale_color_kkplot() +
  theme_kkplot()


## Compare LDNDC ET components

#eva = evacep + evasoil + evasurfacewater + transp

ggplotly(
  ggplot(watercycle_daily %>% subset(year(datetime) == "2018"),aes(x = datetime)) +
    geom_line(aes(y = eva,col = "eva")) +
    geom_area(aes(y = eva,col = "eva")) +
    geom_line(aes(y = transp,col = "transp")) +
    geom_area(aes(y = transp,col = "transp")) +
    geom_line(aes(y = evasoil, col = "evasoil")) +
    geom_area(aes(y = evasoil, col = "evasoil")) +
    geom_line(aes(y = evacep,col = "evacep")) +
    geom_area(aes(y = evacep,col = "evacep")) +
    geom_area(aes(y = evasurfacewater,col = "evasurfacewater")) +
    geom_line(aes(y = evasurfacewater,col = "evasurfacewater")) +
    theme_minimal())


### ### ### ### ### ### ### ### ###
### Water Use Efficiency       ####

# Calculate WUE as GPP/ET (& convert units to gC/m2h -> gC/m2d for GPP and mm/h mm/d)
WUE <-  
GPP %>% 
mutate(GPP = (GPP*60*60)*12/1000000) %>% #µmol/m²s -> g/m²h (ET in mm bzw kg/m²/h)
full_join(.,ET) %>% 
  mutate(date = date(datetime)) %>%
  full_join(.,croptime) %>% relocate(datetime,crop,rep,dof,everything())  %>% select(-date) %>%
  mutate(ET = ET,
         GPP = GPP,
         WUE = GPP/ET)

# WUE plot 
WUE %>%
  #filter(year(datetime) %in% c(2017,2018)) %>%
  #mutate(cycl = factor(paste0(crop,rep))) %>% 
  ggplot(aes(datetime)) +
  geom_point(aes(y = WUE)) +
  #geom_line(aes(y = GPP,color = "GPP")) +
  #geom_line(aes(y = ET,color = "ET")) +
  labs(y = "WUE",color = "") +
  #facet_grid(name~.) +
  #scale_x_datetime(breaks = "1 year",date_labels = "%Y") +
  scale_y_continuous(limits = c(-200,1000)) +
  theme(legend.position = c(.9,.9)) +
  theme_minimal()


# Daily WUE 
WUEd <- 
  full_join(compare_GPPd,compare_ETd) %>%
  mutate(WUE_EC = GPP_EC/ET_EC,
         WUE_ldndc = GPP_ldndc/ET_ldndc)

# Plot Daily WUE, EC vs. LDNDC
ggplot(WUEd,aes(datetime)) +
  geom_point(aes(y = WUE_EC,col = "EC")) +
  geom_point(aes(y = WUE_ldndc,col = "ldndc")) 

# Daily GPP vs. ET 

ggplot(WUEd) +
  geom_point(aes(y = -GPP_ldndc,x = ET_ldndc,col = "ldndc")) +
  geom_point(aes(y = GPP_EC,x = ET_EC, col = "EC"))

### ### ### ### ### ### ### ### ###
### Soil Water Content         ####

plot_SWC <-
compare %>%
  pivot_longer(cols = contains("cm"),names_to = "method") %>%
  mutate(depth = case_when(str_detect(method,"5cm") ~ "5cm",
                           str_detect(method,"10") ~ "10cm",
                           str_detect(method,"20") ~ "20cm")) %>%
  mutate(method = case_when(str_detect(method,"EC")  ~ "EC",
                          str_detect(method,"ldndc") ~ "ldndc")) %>%
  ggplot(aes(date,value,color = method)) +
  facet_grid(factor(depth,levels = c("5cm","10cm","20cm"))~.) +
  geom_point() +
  scale_x_date(breaks = "1 year",date_labels = "%Y") +
  labs(y = "SWC (%)") +
  theme_minimal()



ggsave(paste0("../",prefix,"/SWC.png"),width = 20, height = 5,bg = "white")

### ### ### ### ### ### ### ### ###
### Above Ground Biomass       ####

compare_biomass %>% select(-crop,-contains("yield")) %>%
  right_join(.,croptime,by = c("datetime" = "date")) %>%
  pivot_longer(cols = contains("AGB"),names_to = "method",values_to = "AGB") %>%
  mutate(method = case_when(str_detect(method,"obs")  ~ "obs",
                            str_detect(method,"ldndc") ~ "ldndc")) %>%
  ggplot(aes(datetime,AGB,color = method,shape = method)) +
  geom_point() +
  labs(y = "AGB (kg/m-2)", x = "") +
  scale_x_datetime(breaks = "1 year",date_labels = "%Y") +
  theme_kkplot() +
  scale_color_manual(values = c("red","black"),
                     labels = c("obs","ldndc"),
                     breaks = c("obs","ldndc"),
                     name = "") +
  scale_shape_manual(values = c(4,20),
                     labels = c("obs","ldndc"),
                     breaks = c("obs","ldndc"),
                     name = "") 
ggsave(paste0(folder.plots,"AGB_2.png"),width = 20, height = 5,bg = "white")



### ### ### ### ### ### ### ### ###
### Yield                      ####

compare_biomass %>% select(-crop,-contains("AGB")) %>%
  right_join(.,croptime,by = c("datetime" = "date")) %>%
  pivot_longer(cols = contains("yield"),names_to = "method",values_to = "yield") %>%
  mutate(method = case_when(str_detect(method,"obs")  ~ "obs",
                            str_detect(method,"ldndc") ~ "ldndc")) %>%
  ggplot(data,aes(datetime,yield,color = method,shape = method)) +
  geom_point() +
  labs(y = "yield (kg/m-2)", x = "") +
  scale_x_datetime(breaks = "1 year",date_labels = "%Y") +
  theme_kkplot() +
  scale_color_manual(values = c("red","black"),
                     labels = c("obs","ldndc"),
                     breaks = c("obs","ldndc"),
                     name = "") +
  scale_shape_manual(values = c(4,20),
                     labels = c("obs","ldndc"),
                     breaks = c("obs","ldndc"),
                     name = "") 




ggsave(paste0(folder.plots,"yield.png"),width = 20, height = 5,bg = "white")

### ### ### ### ### ### ### ###
## dvs_flush               ####


source("~/Documents/R_scripts/DE_klingenberg/compare_klingenberg.R")
setwd(folder.plots)
 
physiology_daily %>%
  right_join(.,croptime,by = c("datetime" = "date")) %>%
  right_join(.,  compare_biomass %>% select(-crop,-contains("yield")) %>%
               right_join(.,croptime,by = c("datetime" = "date")) %>%
               pivot_longer(cols = contains("AGB"),names_to = "method",values_to = "AGB") %>%
               mutate(method = case_when(str_detect(method,"obs")  ~ "obs",
                                         str_detect(method,"ldndc") ~ "ldndc"))) %>% 
  filter(crop == "maize") %>% #,dof > 120) %>% select(rep,dof,gdd) %>% pivot_wider(names_from = rep,names_prefix = "rep_",values_from = gdd View() 
ggplot(aes(dof)) +
  geom_point(aes(y = AGB,shape = method)) +
  geom_point(aes(y = dvs_flush*3,color = gdd)) +
  facet_grid(rep~crop) +
  theme(legend.position = c(0.1,0.9),legend.background = element_blank())

  
ggsave(filename = "dvs_flush_maize_6.png",width = 4, height = 15)

# ### ### ### ### ### ### ### ###
# Climate                    ####

ggplotly(
  compare_climate %>%
    ggplot(aes(datetime)) +
    geom_point(aes(y = temp_canopy_top,color = "EC")) +
    geom_point(aes(y = tavg,color = "ldndc")) +
    theme_minimal() +
    theme(legend.position = "top") +
    labs(colour = "",y = "T above canopy (°C)",x = "")
)


# ### ### ### ### ### ### ### ###
# ## overview                ####
# 
# layout =  "AABB
#            CCDD
#            EEFF"
# 
# plot_AGB + plot_Reco + plot_NEE + plot_ET + plot_SWC &
#   theme(legend.position = c(1,1)) &
#   theme_ldndc() &
#   theme(strip.text = element_blank()) &
#   scale_x_date(limits =  c(as.Date("2006-10-02"),as.Date("2008-08-30")))










