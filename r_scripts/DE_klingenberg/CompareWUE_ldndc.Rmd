---
title: "Comparison different WUECMAX in LDNDC"
output: html_notebook
---

```{r Libraries, include = FALSE}
## Libraries
library(tidyverse)
library(sp)
library(maptools)
library(knitr)
library(plotly)
library(patchwork)
library(viridis)


knitr::opts_chunk$set(dev = "png",
                      echo = T)
# Show point shapes in R:
#ggpubr::show_point_shapes()
#ggpubr::show_line_types()
multiple_runs <- c("wuedefault","wuediff","wue2diff")


source("~/Documents/R_scripts/DE_klingenberg/compare_klingenberg.R")

theme_kkplot <- function(){theme_minimal() %+replace%
    theme(legend.position = "top",#c(0.8,0.9),
          legend.background = element_blank(),
          legend.direction = "horizontal",
          panel.grid.minor = element_blank())
}
twoeighteen <- c(as_datetime("2018-1-1"),as_datetime("2018-12-31"))

scale_color_kkplot <- function() scale_color_manual(values = c("red","black","blue"),
                                                    labels = c("EC",if_else(length(multiple_runs) > 1,as.character(multiple_runs),"ldndc")),
                                                    breaks = c("EC",if_else(length(multiple_runs) > 1,as.character(multiple_runs),"ldndc")),
                                                    name = "")
```


```{r eval=FALSE, include=FALSE}

crops %>% select(crop,Sowing_date,harvest_date) %>% 
  filter(crop %in% c("maize", "winter wheat")) %>% 
  group_by(crop) %>% kable

```

Modeled WUE with WUECMAX = 8.6 and 10

```{r Compare Water use Efficiency, fig.height=7, fig.width=6, message=TRUE}
WUEd <- full_join(ET_daily,GPP_daily) %>%
  right_join(croptime,by = c("datetime" = "date")) %>%
  rename_at(vars(contains("_daily")),~str_remove(.,"_daily")) %>%
  mutate(WUE = GPP/ET)


# 

long_WUEd %>%
  filter(crop == "maize",dof > 30) %>%
  ggplot() +
  geom_point(aes(ET,GPP,color = dof)) +
  theme_minimal() +
  facet_grid(run~rep,labeller = as_labeller(c('1' = '2007','2' = 2012,'3' = 2018,"EC" = "EC",
                                              "wuediff" = "WUECMAX = 10","wue2diff" = "WUEMAX = 4","wuedefault" = "WUECMAX = 8.7"))) +
  scale_color_viridis(limits = c(0,160)) +
  labs(col = "days on field",title = "Maize, daily",x = "ET (mm/d)",y = "GPP ( gC/m2)")
```




