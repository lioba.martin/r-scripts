# Info:
# File Purpose: Create simple plots of the output of the LandscapeDNDC Simulations 
# Author: Lioba Martin
# Contact: lioba.martin@kit.edu
# Created: 08.06.2022
# Last Changed: 09.06.2022
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# Things that need to be adapted: 

#prefix <- "DE_gebesee"
#multiple_runs <- c("metrx")
prefix <- "US_maricopa"
multiple_runs <- c("F1_H","F1_C","S4_H","S4_C")
multiple_runs <- c("F1_C","S4_C")

#prefix <- "CH_Reckenholz"
#multiple_runs <- c("")

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# General Variables and Packages
Sys.setlocale("LC_TIME","English")

library(ggplot2)
library(tidyverse)
library(patchwork) # Arrange plots: patchwork
library(lubridate)

## LandscapeDNDC Model Output 

# This script reads in the simulation output and, if there are multiple runs, aggregates them into one table
# The output is stored in R tibbles named after the output, but with _ instead of - (e.g. microclimate_daily for the output table microclimate-daily)
# For multiple simulation runs the column "run" includes the name of the run given as the suffix to prefix in the output folder
# Additionally, a list "list.vars" is created, with infos about the variable name, unit, and which output file it is stored in
source("~/Documents/R_scripts/simulation_output_to_R.R")



## Define a folder where the plots go 

folder.plots <- paste0(getwd(),"/",prefix,"_plots/")
if(!dir.exists(folder.plots)) {dir.create(folder.plots); message("create ",folder.plots)}

## Plot Selected output Variables together ####

# Select key variables to plot and select the style (p = point, l = line, h = columns)
key.vars <- tribble(~variable,~style,
                    "vc_act_25","p",
                    "temp_above_canopy_avg","l",
                    "prec","h",
                    "irri","h",
                    "DW_above","p",
                    "DW_fru","p",
                    "NC_fru","p",
                    "dvs_flush","p",
                    "soilwater_10cm","p",
                    "dN_no3_leach","h")

#create a table with the units of the key variables from "list.vars"
table <- left_join(key.vars,list.vars,by = "variable") %>% filter(table != "report-harvest")



# Plot the Key variables 
for(t in table$variable){
  info <- table %>% select(variable,style,unit,table) %>% unique %>% filter(variable == t)
  data <- info$table %>% unique %>% str_replace("-","_") %>% get 
  
  plot <-
    data %>% 
    ggplot(aes(x = date,color = run,fill = run)) +
    labs(y = paste(info$variable," \n (",info$unit,")",sep = ""),
         x = "") +
    theme_minimal()
  
  if(info$style == "h")plot <- plot + geom_col(aes_string(y = t)) 
  if(info$style == "l")plot <- plot + geom_line(aes_string(y = t),size = 1) 
  if(info$style == "p")plot <- plot + geom_point(aes_string(y = t),size = 0.6)
  
  if(length(multiple_runs) == 1) plot <- plot + theme(legend.position="none")
  assign(t,plot)
  rm(plot)
}


# Identify the length of the simulation 
load("C:/Users/martin-l/Documents/R_scripts/functions/daterange.RData")

daterange <- date_range(spinup_years = 3,date_vec = physiology_daily$date)

# Use the patchwork package to group multiple plots 
(((temp_above_canopy_avg /dvs_flush / DW_above / DW_fru /vc_act_25) |(NC_fru /prec /irri  / soilwater_10cm / dN_no3_leach)) +
    plot_layout(guides = 'collect')) & scale_x_date(limits =  daterange) &
  if(length(multiple_runs) == 1) theme(legend.position="none")

ggsave(paste0(folder.plots,"Key_Plots_1.png"),width = 20, height = 14)


## Plot all variables, but for all runs together

  for(v in unique(list.vars %>% pull(variable))) {
    info <- list.vars %>% select(variable, unit, table) %>% unique %>% filter(variable == v) 
    
    for(tab in unique(info$table)){
      data <- tab %>%  str_replace("-","_") %>% get 
      folder <- paste0(folder.plots,tab,"/")
      if(!dir.exists(folder)) dir.create(folder,recursive = T)
      message(paste(tab,info$variable,sep = " "))
      plot <- 
      ggplot(data,aes(date,color = run)) +
        geom_point(aes_string(y = v),alpha = 0.9) +
        labs(y = paste0(v," (",info$unit,")")) +
        #scale_x_date(limits = c(as.Date("2007-01-01"),as.Date("2009-12-31"))) +
        theme_minimal()
      if(length(multiple_runs) == 1) plot <- plot + theme(legend.position="none")
      
      ggsave(plot,filename = paste(folder,v,".png", sep = ""),width = 7, height = 4,unit = "in")
    }
  }


## Plot every variable, with every run separately ####
# This only makes sense if there are multiple runs

if(length(multiple_runs) != 1){

  for (r in list.vars %>% pull(run) %>% unique){
    for(v in unique(list.vars %>% pull(variable))) {
      info <- list.vars %>% filter(variable ==v) %>% select(variable, unit, table) %>% unique 
      for(tab in unique(info$table)){
        data <- tab %>%  str_replace("-","_") %>% get  %>% filter(run == r)
        
        folder <- paste(prefix,"_",r,"/plots/",tab,"/",sep ="")
        if(!dir.exists(folder)) dir.create(folder,recursive = T)
        message(paste(r,tab,info$variable,sep = " "))
        plot <- 
          ggplot(data,aes(date,color = run)) +
          geom_point(aes_string(y = v),alpha = 0.9) +
          labs(y = paste(v," (",info$unit,")",sep = "")) +
          #scale_x_date(limits = c(as.Date("2007-01-01"),as.Date("2009-12-31"))) +
          theme_minimal() + theme(legend.position="none")
        ggsave(plot,filename = paste(folder,v,".png", sep = ""),width = 7, height = 4,unit = "in")
      }
    }
  }
}


