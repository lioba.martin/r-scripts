
library(ggplot2)
library(tidyverse)
# To plot next to each other: patchwork
library(patchwork)

## LandscapeDNDC Model Output 
prefix <- "CH_reckenholz"
multiple_runs <- F

source("~/Documents/R_scripts/simulation_output_to_R.R")

## Plot Key Variables 
# DW_fru -> Dry weight fruit (yield)
# Soil water content
# Plant development
# Leeching 

# vc_act_25 -> Rubisco Activity
## dvs_flush -> Relative state of foliage flushing 

key.vars <- tribble(~variable,~style,
                    "vc_act_25","p",
                    "temp_above_canopy_avg","p",
                    "prec","h",
                    "DW_above","p",
                    "DW_fru","p",
                    "NC_fru","p",
                    "dvs_flush","p",
                    "soilwater_10cm","p",
                    "dN_no3_leach","h")

table <- left_join(key.vars,list.vars,by = "variable") %>% filter(table != "report-harvest")

for(t in table$variable){
  info <- table %>% filter(variable == t)
  
  data <- read.delim(info$file) 
  colnames(data) <- c("source","id","datetime",list.vars %>% filter(table == info$table) %>% pull(variable))

  data <- data %>% mutate(date = as.Date(datetime,tz = "UTC")) #%>%  select(date,!!t)
 
  plot <-
    data %>% 
    ggplot(aes(x = date)) +
    labs(y = paste(info$variable," \n (",info$unit,")",sep = ""),
         x = "")
  
    if(table %>% filter(variable == t) %>% pull(style) == "h")plot <- plot + geom_col(aes_string(y = t),color = "blue")
    if(table %>% filter(variable == t) %>% pull(style) == "l")plot <- plot + geom_line(aes_string(y = t),size = 1) 
    if(table %>% filter(variable == t) %>% pull(style) == "p")plot <- plot + geom_point(aes_string(y = t),size = 0.6)
   
  assign(t,plot)
}



((temp_above_canopy_avg /dvs_flush / DW_above / DW_fru ) |(NC_fru /prec/vc_act_25 / soilwater_10cm / dN_no3_leach)) & 
  theme_minimal()  
  scale_x_date(limits = c(as.Date("2015-01-01"),as.Date("2015-12-31"))) 

ggsave("plots/overview1.png",width = 10, height = 7)




## Plot all variables

for(i in 1:nrow(df)) {
  data <- read.table(df$result.file[i],header = T,sep = "\t",check.names = T,na.strings = -99.99) 
  names <-  str_split_fixed(names(data),pattern = "[.]",Inf)[,1]
  unit <-  names(data) %>% str_split_fixed(pattern = "[.]",Inf) %>% .[-c(1:3),c(-1)] %>% apply(1,paste,collapse = "")
  colnames(data) <- names
  names <- names[-c(1:3)]
  
  data$datetime <- as.POSIXct(data$datetime,tz = "UTC")
  
  folder <- paste("plots",df$result.name[i],sep ="/")
  
  if(!dir.exists(folder)) dir.create(folder,recursive = T)
  
  
  for (n in 1:length(names)){
    
    ggplot(data,aes(datetime)) +
      geom_line(aes_string(y =names[n])) +
      labs(y = paste(names[n]," (",unit[n],")",sep = ""))
    
    ggsave(filename = paste(folder,"/",names[n],".png", sep = ""),width = 7, height = 4,unit = "in")
    
  }
  print(paste(df$result.name[i],sep = "/"))
}

