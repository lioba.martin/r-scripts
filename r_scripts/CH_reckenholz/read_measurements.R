# Info:
# File Purpose: Read the measured data from the Agroscope Lysimeters in Reckenholz
Author <-  "Lioba Martin"
Contact <-  "lioba.martin@kit.edu"
# Created: 15.06.2022
# Last Changed: 21.07.2022
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

# Things that need to be changed:
# What is the prefix used for the project:
prefix <- "CH_reckenholz"
# Where are the "raw" measured data?
input <- "C:/Users/martin-l/Documents/LandscapeDNDC/input/CH_reckenholz/"

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


## Path where the standard-format measurement data should be stored
path.measurements <- paste0("C:/Users/martin-l/Documents/ldndc_work/measurements/arable/",prefix)
if(!dir.exists(path.measurements)) dir.create(path.measurements)

## load necessary libraries 
library(tidyverse)
library(lubridate)
## Load self written functions
# write.html -> convert a table of measurements to html with header, needs table and units
load("C:/Users/martin-l/Documents/R_scripts/functions/write_measurements_to_html.Rdata")


## ## ## ## ##
# Evapotranspiration from the Lysimeters #####
# Set Working Directory
setwd(input)


LysET <- "Lys2_2015_ET_Stunde.xlsx" %>% readxl::read_excel(skip = 1) %>% 
  janitor::clean_names() %>% slice(-1) %>% mutate_if(is.character,as.numeric) %>%
  mutate(date = date(date_time)) %>% group_by(date) %>% 
  summarise(actual_et = mean(actual_et,na.rm =T ),actual_et_sd = sd(actual_et,na.rm =T ),pet_pm = mean(pet_penman_monteith,na.rm=T),pet_pm_sd = sd(pet_penman_monteith,na.rm=T))
LysET




## Convert to html and export 

# convert to html with header, needs input table (with first column being the date column) and units for the columns in seperate vector
setwd(path.measurements)
LysET <- write.html(LysET,units = rep("mm",4),paste0(prefix,"_ET.txt"))

# export with appropirate suffix
write.table(LysET,file = paste0(prefix,"_ET.txt"),quote = F,col.names = F,row.names = F,na = "",sep = "\t")


## ## ## ## ## ## ## ## ##
setwd(input)
# Other Lysimeter data


sheets <- c("Weight","Kipp","FDR","EQ","Temp","Precip")
for (s in c(1:6)){
  Lys <- "Lys2_2015_Sonden_Stunde.xlsx" %>% readxl::read_excel(skip = 1,sheet = paste0("data_",s)) %>% slice(-1) %>% 
    janitor::clean_names() %>% mutate_if(is.character,as.numeric) %>%
    mutate(date = date(date_time)) %>% group_by(date) %>% select(-date_time) %>%
    summarise(across(everything(), list(mean = mean, sd = sd)))
  assign(paste0("Lys",sheets[s]),Lys)
}

LysWeight <- write.html(LysWeight,c("kg","kg")) ## Weight ## 
LysKipp   <- write.html(LysKipp,c("l","l")) ## Kipp ## 
LysFDR    <- write.html(LysFDR,rep("%",16)) ## FDR ##
LysEQ     <- write.html(LysEQ,rep("kPa",16)) ## EQ ##
LysTemp   <- write.html(LysTemp,rep("C",16)) ## Temperature ##
LysPrecip <- write.html(LysPrecip,c("mm","mm")) ## Precipitation

setwd(path.measurements)

for(s in sheets){
  
  
  write.table(get(paste0("Lys",s)), file = paste0(prefix,"_",s,".txt"),quote = F,col.names = F,row.names = F,na = "",sep = "\t")
}


## Yield Data ### 
setwd(input)

yield <- read.csv2("Kulturmassnahmen2015.csv",encoding = "UTF-8") %>% janitor::clean_names() %>% tibble %>%
  mutate(date = as.Date(datum,format = "%d.%m.%Y")) %>%
  filter(massnahmen == "Ernte" & !is.na(menge)) %>% select(date,einheit,menge) %>% 
  rename(yield = menge,unit = einheit) 


yield <- write.html(yield %>% select(-unit),"dt TS/ha")

setwd(path.measurements)
write.table(yield,file = paste0(prefix,"_yield.txt"),quote = F,col.names = F,row.names = F,na = "",sep = "\t")

